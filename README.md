> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Kristopher Mangahas

### Project 1 # Requirements

*Deliverables:*

1. Provide Bitbucket read-only access to p1 repo, *must* include README.md, using Markdown syntax
2. Blackboard Links: p1 Bitbucket repo



#### Assignment Screenshots:

*Screenshot of a failed validation*:

![JDK Installation Screenshot](img/fail.png)


*Screenshot of empty inputs*:

![Tomcat Installation Screenshot](img/empty.png)


*Screenshot of successful validation*:

![Tomcat Installation Screenshot](img/win.png)

